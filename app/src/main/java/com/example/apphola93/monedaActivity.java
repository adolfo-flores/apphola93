package com.example.apphola93;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class monedaActivity extends AppCompatActivity {
    private EditText txtCantidad;
    private TextView txtResultado;
    private Spinner spnMoneda;
    private Button btnCalcular, btnCerrar, btnLimpiar;
    private int pos=0; //seleccionar la posicion del Spinner
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_moneda);
        iniciarComponentes();

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtCantidad.getText().toString().matches("")){
                    Toast.makeText(getApplicationContext(),
                            "Faltó capturar cantidad",Toast.LENGTH_SHORT).show();
                }
                else{
                    float cantidad = Float.parseFloat(txtCantidad.getText().toString());
                    float resultado = 0.0f;
                   // cantidad = 0.0f;
                    switch(pos){
                        case 0: //pesos a dolar americano
                            resultado = cantidad * 0.06f;
                            break;
                        case 1: //pesos a dolar canadiense
                            resultado = cantidad * 0.082f;
                            break;
                        case 2: //pesos a euro
                            resultado = cantidad * 0.055f;
                            break;
                        case 3: //peso a libra
                            resultado = cantidad * 0.047f;
                            break;
                    }
                    txtResultado.setText(String.format("%.2f",resultado));
                }


            }
        });

        spnMoneda.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                pos = position;
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }


        });


        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtResultado.setText("");
                txtCantidad.setText("");
            }
        });

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }

    public void iniciarComponentes(){
        txtCantidad = (EditText) findViewById(R.id.txtCantidad);
        txtResultado = (TextView) findViewById(R.id.txtResultado);
        spnMoneda = (Spinner) findViewById(R.id.spnMoneda);
        btnCalcular = (Button) findViewById(R.id.btnCalcular);
        btnCerrar = (Button) findViewById(R.id.btnCerrar);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);

        //Generar el Adaptador, para llenarlo con el ArrayString
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_expandable_list_item_1,
                getResources().getStringArray(R.array.moneda));


        spnMoneda.setAdapter(adapter);
    }
}