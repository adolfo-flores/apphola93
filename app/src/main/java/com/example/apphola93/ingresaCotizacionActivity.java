package com.example.apphola93;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.snackbar.Snackbar;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.apphola93.databinding.ActivityIngresaCotizacionBinding;

public class ingresaCotizacionActivity extends AppCompatActivity {
/*
    private AppBarConfiguration appBarConfiguration;
    private ActivityIngresaCotizacionBinding binding;
*/
    private EditText txtUsuario;
    private Button btnIngresar, btnCerrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_ingresa_cotizacion);
        txtUsuario = (EditText) findViewById(R.id.txtUsuario);
        btnIngresar = (Button) findViewById(R.id.btnIngresar);
        btnCerrar = (Button) findViewById(R.id.btnCerrar);

        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(txtUsuario.getText().toString().matches("")){
                    Toast.makeText(ingresaCotizacionActivity.this,
                            "Falto capturar el usuario", Toast.LENGTH_SHORT).show();
                }
                else{

                    Intent intent = new Intent(ingresaCotizacionActivity.this, CotizacionActivity.class);
                    intent.putExtra("cliente", txtUsuario.getText().toString());
                    startActivity(intent);

                }
            }
        });

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }



}