package com.example.apphola93;

import java.io.Serializable;
import java.util.Random;

public class Cotizacion implements Serializable {
    private int folio;
    private String descripcion;
    private float valorAuto;
    private float porcentajePagoInicial;
    private int plazos;

    public Cotizacion(int folio, String descripcion, float valorAuto, float porcentajePagoInicial, int plazos) {
        this.folio = folio;
        this.descripcion = descripcion;
        this.valorAuto = valorAuto;
        this.porcentajePagoInicial = porcentajePagoInicial;
        this.plazos = plazos;
    }

    public Cotizacion(){
        this.folio = 0;
        this.descripcion = "";
        this.valorAuto = 0.0f;
        this.porcentajePagoInicial = 0.0f;
        this.plazos = 0;
    }

    public int getFolio() {
        return folio;
    }

    public void setFolio(int folio) {
        this.folio = folio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public float getValorAuto() {
        return valorAuto;
    }

    public void setValorAuto(float valorAuto) {
        this.valorAuto = valorAuto;
    }

    public float getPorcentajePagoInicial() {
        return porcentajePagoInicial;
    }

    public void setPorcentajePagoInicial(float porcentajePagoInicial) {
        this.porcentajePagoInicial = porcentajePagoInicial;
    }

    public int getPlazos() {
        return plazos;
    }

    public void setPlazos(int plazos) {
        this.plazos = plazos;
    }

    //metodos de comportamiento
    public int generarFolio(){
        Random r = new Random();
        return r.nextInt()%1000;
    }

    public float calcularPagoInicial(){
        return this.valorAuto * (this.porcentajePagoInicial/100);
    }

    public float calcularPagoMensual(){
        return (valorAuto - this.calcularPagoInicial())/this.plazos;
    }
}
