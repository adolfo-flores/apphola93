package com.example.apphola93;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class CotizacionActivity extends AppCompatActivity {

    private TextView txtUsuario, txtPagoInicial, txtPagoMensual, txtFolio;
    private EditText txtDescripcion, txtValor, txtPorcentaje;
    private RadioGroup radioGroup;
    private RadioButton radio12, radio18, radio24, radio36;
    private Button btnCalcular, btnLimpiar, btnCerrar;
    private int folio;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_cotizacion);
        iniciarComponentes();

        //Generar folio y mostarlo
        Cotizacion cotizacion = new Cotizacion();

        while (folio <= 0){
            folio = cotizacion.generarFolio();
        }
        txtFolio.setText(String.format("Folio: %d", folio));

        //Recibir la variable de cliente desde ingresaCotizacion.java
        Intent intent = getIntent();
        String cliente = intent.getStringExtra("cliente");
        //Asignar el texto al Textview
        if (cliente != null) {
            txtUsuario.setText(cliente);
        } else {
            Toast.makeText(CotizacionActivity.this, "No se recibió el usuario", Toast.LENGTH_SHORT).show();
        }


        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtDescripcion.getText().toString().matches("") ||
                        txtValor.getText().toString().matches("") ||
                        txtPorcentaje.getText().toString().matches("") ||
                        radioGroup.getCheckedRadioButtonId() == -1){
                    Toast.makeText(getApplicationContext(),"FALTAN AGREGAR DATOS", Toast.LENGTH_SHORT).show();
                }
                else{
                    calcular();
                }
            }
        });

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtDescripcion.setText("");
                txtValor.setText("");
                txtPorcentaje.setText("");
                radioGroup.clearCheck();
                txtPagoInicial.setText("");
                txtPagoMensual.setText("");
            }
        });


    }

    public void iniciarComponentes(){
        txtUsuario = (TextView) findViewById(R.id.txtUsuario);
        txtPagoInicial = (TextView) findViewById(R.id.txtPagoInicial);
        txtPagoMensual = (TextView) findViewById(R.id.txtPagoMensual);
        txtFolio = (TextView) findViewById(R.id.txtFolio);

        txtDescripcion = (EditText) findViewById(R.id.txtDescripcion);
        txtValor = (EditText) findViewById(R.id.txtValor);
        txtPorcentaje = (EditText) findViewById(R.id.txtPorcentaje);

        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        radio12 = (RadioButton) findViewById(R.id.radio12);
        radio18 = (RadioButton) findViewById(R.id.radio18);
        radio24 = (RadioButton) findViewById(R.id.radio24);
        radio36 = (RadioButton) findViewById(R.id.radio36);

        btnCalcular = (Button) findViewById(R.id.btnCalcular);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnCerrar = (Button) findViewById(R.id.btnCerrar);
    }


  //  @SuppressLint("DefaultLocale")
    private void calcular() {
        String descripcion = txtDescripcion.getText().toString();
        float valorAuto = Float.parseFloat(txtValor.getText().toString());
        float porcentajePagoInicial = Float.parseFloat(txtPorcentaje.getText().toString());
        int plazos = 0;



        if(radioGroup.getCheckedRadioButtonId() == R.id.radio12){
            plazos = 12;
        }
        else if (radioGroup.getCheckedRadioButtonId() == R.id.radio18){
            plazos = 18;
        }
        else if (radioGroup.getCheckedRadioButtonId() == R.id.radio24){
            plazos = 24;
        }
        else if (radioGroup.getCheckedRadioButtonId() == R.id.radio36){
            plazos = 36;
        }

        Cotizacion cotizacion = new Cotizacion(0, descripcion, valorAuto, porcentajePagoInicial, plazos);
        float pagoInicial = cotizacion.calcularPagoInicial();
        float pagoMensual = cotizacion.calcularPagoMensual();

        txtPagoInicial.setText(String.format("Pago Inicial: %.2f", pagoInicial));
        txtPagoMensual.setText(String.format("Pago Mensual: %.2f", pagoMensual));
    }


}