package com.example.apphola93;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

import org.w3c.dom.Text;

public class gradosActivity extends AppCompatActivity {
    private EditText txtGrados;
    private TextView txtResultado;
    private Button btnCalcular, btnLimpiar, btnCerrar;
    private RadioButton radioCelsiusFahrenheit, radioFahrenheitCelsius;
    private RadioGroup radioGroup;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_grados);
        iniciarComponentes();
        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Obtener el id del RadioButton seleccionado
                //int selectedId = radioGroup.getCheckedRadioButtonId();
                //validar
                //No se capturaron los grados
                if(txtGrados.getText().toString().matches("")){
                    Toast.makeText(getApplicationContext(),"Por favor introduzca los grados",Toast.LENGTH_SHORT).show();
                }
                else{
                    //Ninguna opcion fue seleccionada
                    if(radioGroup.getCheckedRadioButtonId() == -1){
                        Toast.makeText(getApplicationContext(),"Seleccione una opcion", Toast.LENGTH_SHORT).show();
                    }
                    else{
                        if(radioGroup.getCheckedRadioButtonId() == R.id.radioCelsiusFahrenheit){
                            //Celsius a Fahrenheit
                            float grados = Float.parseFloat(txtGrados.getText().toString());
                            grados = (grados * 9/5) + 32;
                            txtResultado.setText(String.valueOf(grados));
                        }
                        else{
                            //Fahrenheit a Celsius
                            float grados = Float.parseFloat(txtGrados.getText().toString());
                            grados = (grados - 32) * 5/9;
                            txtResultado.setText(String.valueOf(grados));
                        }
                    }
                }
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtResultado.setText("");
                txtGrados.setText("");
            }
        });

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }

    public void iniciarComponentes(){
        txtGrados = (EditText) findViewById(R.id.txtGrados);
        txtResultado = (TextView) findViewById(R.id.txtResultado);
        btnCalcular = (Button) findViewById(R.id.btnCalcular);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnCerrar = (Button) findViewById(R.id.btnCerrar);
        radioCelsiusFahrenheit = (RadioButton) findViewById(R.id.radioCelsiusFahrenheit);
        radioFahrenheitCelsius = (RadioButton) findViewById(R.id.radioFahrenheitCelsius);
        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
    }
}